package com.gmail.leslie.stroh.rdb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.gmail.leslie.stroh.accounts.Account;
import com.gmail.leslie.stroh.accounts.Account.Status;
import com.gmail.leslie.stroh.accounts.AccruableInterest;
import com.gmail.leslie.stroh.accounts.CheckingAccount;
import com.gmail.leslie.stroh.accounts.SavingsAccount;
import com.gmail.leslie.stroh.users.Customer;
import com.gmail.leslie.stroh.users.Employee;
import com.gmail.leslie.stroh.users.User;

public class BankingDaoImpl implements BankingDataAccessObjectLayer {
	private static Logger log = Logger.getRootLogger();
	
	
	@Override
	public boolean checkUserNameAvailability(String username) {
		String sql = "SELECT USERNAME FROM BANK_USER bu WHERE bu.USERNAME = ?";
		try(Connection connection = ConnectionUtil.getConnection();
			PreparedStatement pstatement = connection.prepareStatement(sql)){
			pstatement.setString(1, username);
			ResultSet userSet = pstatement.executeQuery();
			return !userSet.next();
		}
		catch(SQLException e) {
			e.printStackTrace();
			log.error("Failed to connect to database at BankingDaoImpl.checkUserNameAvailability");
			return false;
		}
		
	}
	
	@Override
	public User getUserFromLoginInfo(String role, String username, String password) {
		String sql = "SELECT * FROM BANK_USER bu WHERE bu.USERNAME = ?";
		try(Connection connection = ConnectionUtil.getConnection();
		    PreparedStatement pstatement = connection.prepareStatement(sql)){
			pstatement.setString(1, username);
			ResultSet userSet = pstatement.executeQuery();
			for(boolean isValid = userSet.next(); isValid; isValid = userSet.next()) {
				if(!role.equals(userSet.getString("USER_ROLE"))) return null;
				if(!password.equals(userSet.getString("PASSWORD"))) return null;
				if(role.equals("EMPLOYEE")) {
					return new Employee(userSet.getLong("USER_ID"),
							            userSet.getString("USERNAME"),
							            userSet.getString("PASSWORD"));
				}
				else if(role.equals("CUSTOMER")) {
					Customer customer = new Customer(userSet.getLong("USER_ID"),
													 userSet.getString("USERNAME"),
													 userSet.getString("PASSWORD"));
					customer.setBankAccounts(getAccountsFromUserId(userSet.getLong("USER_ID")));
					return customer;
				}
			}
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	

	public List<Account> getAccountsFromUserId(Long id) {
		String sql = "SELECT * FROM ACCOUNT acc WHERE acc.ACCOUNT_OWNER = ?";
		try(Connection connection = ConnectionUtil.getConnection();
		    PreparedStatement pstatement = connection.prepareStatement(sql)){
			pstatement.setLong(1, id);
			ResultSet accountSet = pstatement.executeQuery();
			return createListOfAccounts(accountSet);
			
		}
		catch(SQLException e) {
			e.printStackTrace();
			log.error("SQLException at BankingDaoImpl.getAccountsFromUserId");
		}
		return null;
	}

	@Override
	public long getMaxUserId() {
		return getMax("SELECT MAX(USER_ID) FROM BANK_USER");
	}


	@Override
	public long getMaxAccountId() {
		return getMax("SELECT MAX(ACCOUNT_ID) FROM ACCOUNT");
	}
	
	private long getMax(String sql) {
		try(Connection connection = ConnectionUtil.getConnection();
				Statement stmt = connection.createStatement()	){
				ResultSet max = stmt.executeQuery(sql);
				if(!max.next()) return 1;
				else return max.getLong(1);
				
		}catch (SQLException e) {
			e.printStackTrace();
			log.error("SQLException at BankingDaoImpl.getMax");
		}
		return -1;
	}


	@Override
	public boolean insertNewUser(String username, String password) {
		long id = getMaxUserId() + 1;
		String sql = "INSERT INTO BANK_USER values(?,?,?,?)";
		try(Connection connection = ConnectionUtil.getConnection();
		    PreparedStatement pstatement = connection.prepareStatement(sql)){
			pstatement.setLong(1,id); 
			pstatement.setString(2, "CUSTOMER");
			pstatement.setString(3, username);
			pstatement.setString(4, password);
			int numUpdated = pstatement.executeUpdate();
			return numUpdated == 1;
		}
		catch(SQLException e) {
			log.error("SQLException at BankingDaoImpl.insertNewUser");
			return false;
		}
	}


	@Override
	public boolean createNewAccount(long customerId, String accountType, Account newAccount) {
		long id = getMaxAccountId() + 1;
		newAccount.setAccountNumber(id);
		double interestRate = determineInterestRate(newAccount);
		String sql = "INSERT INTO ACCOUNT values(?,?,?,?,?,?)";
		try(Connection connection = ConnectionUtil.getConnection();
		    PreparedStatement pstatement = connection.prepareStatement(sql)){
			pstatement.setLong(1,id); 
			pstatement.setString(2, accountType);
			pstatement.setDouble(3, newAccount.getBalance());
			pstatement.setString(4, "PENDING");
			pstatement.setDouble(5, interestRate);
			pstatement.setLong(6, customerId);
			int numUpdated = pstatement.executeUpdate();
			return numUpdated == 1;
		}
		catch(SQLException e) {
			log.error("SQLException at BankingDaoImpl.createNewAccount");
			return false;
		}
	}
	

	@Override
	public boolean updateAccountValue(long accountID, double toValue) {
		String sql = "UPDATE ACCOUNT SET BALANCE = ? WHERE ACCOUNT_ID = ?";
		try(Connection connection = ConnectionUtil.getConnection();
			PreparedStatement pstatement = connection.prepareStatement(sql)){
			pstatement.setDouble(1, toValue);
			pstatement.setLong(2, accountID);
			return pstatement.executeUpdate() == 1;
		}
		catch(SQLException e) {
			e.printStackTrace();
			log.error("SQLException at BankingDaoImpl.updateAccountValue");
			return false;
		}
	}
	
	@Override
	public List<Customer> getCustomerWithPendingAccounts() {
		String sql = 
				  "SELECT USER_ID,USERNAME,ACCOUNT_ID,ACCOUNT_TYPE,BALANCE,INTEREST_RATE,APPROVED FROM BANK_USER bu \r\n"
				  + "JOIN ACCOUNT acc \r\n"
				  + "ON acc.ACCOUNT_OWNER = USER_ID\r\n"
				  + "WHERE USER_ROLE = 'CUSTOMER' AND acc.APPROVED = 'PENDING'";
		try(Connection connection = ConnectionUtil.getConnection();
			Statement stmt = connection.createStatement()){
			ResultSet customerSet = stmt.executeQuery(sql);
			return accountCustomerJoinProcessing(customerSet);
			
		}catch(SQLException e) {
			log.error("SQLException at BankingDaoImpl.getCustomerWithPendingAccounts");
			e.printStackTrace();
		}
		return null;
	}
	
	
	@Override
	public boolean updateAccountStatus(long accountID, Status newStatus) {
		String sql = "UPDATE ACCOUNT SET APPROVED = ? WHERE ACCOUNT_ID = ?";
		try(Connection connection = ConnectionUtil.getConnection();
			PreparedStatement pstatement = connection.prepareStatement(sql)){
			pstatement.setString(1, newStatus.name());
			pstatement.setLong(2, accountID);
			return pstatement.executeUpdate() == 1;
		}
		catch(SQLException e) {
			e.printStackTrace();
			log.error("SQLException at BankingDaoImpl.updateAccountStatus");
			return false;
		}
	}
	
	@Override
	public Customer getCustomerFromUsername(String username) {
		String sql = "SELECT * FROM BANK_USER bu WHERE bu.USERNAME = ?";
		try(Connection connection = ConnectionUtil.getConnection();
		    PreparedStatement pstatement = connection.prepareStatement(sql)){
			pstatement.setString(1, username);
			ResultSet userSet = pstatement.executeQuery();
			for(boolean isValid = userSet.next(); isValid; isValid = userSet.next()) {
				String role = userSet.getString("USER_ROLE");
				if(role.equals("EMPLOYEE")) return null;
				else if(role.equals("CUSTOMER")) {
					Customer customer = new Customer(userSet.getLong("USER_ID"),
													 userSet.getString("USERNAME"),
													 userSet.getString("PASSWORD"));
					customer.setBankAccounts(getAccountsFromUserId(userSet.getLong("USER_ID")));
					return customer;
				}
			}
		}
		catch(SQLException e) {
			e.printStackTrace();
			log.error("SQLException at BankingDaoImpl.getCustomerFromUsername");
			
		}
		return null;
	}
	

	
	private double determineInterestRate(Account account) {
		if(account instanceof AccruableInterest) {
			return ((AccruableInterest)account).getInterestRate();
		}
		return 0.00;
	}
	
	private List<Customer> accountCustomerJoinProcessing(ResultSet customerSet) throws SQLException{
		HashMap<Long,Customer> customers = new HashMap<Long,Customer>();
		for(boolean isValid = customerSet.next(); isValid; isValid = customerSet.next()) {
			long id = customerSet.getLong("USER_ID");
			Account acc = createAccountFromResultSet(customerSet);
			if(customers.containsKey(id)) {
				customers.get(id).getBankAccounts().add(acc);
			}
			else {
				Customer newCustomer = new Customer();
				newCustomer.setID(customerSet.getLong("USER_ID"));
				newCustomer.changeUserName(customerSet.getString("USERNAME"));
				newCustomer.getBankAccounts().add(acc);
				customers.put(newCustomer.getID(), newCustomer);
			}
		}
		return new ArrayList<Customer>(customers.values());
	}
	

	private List<Account> createListOfAccounts(ResultSet accountSet) throws SQLException{
		List<Account> result = new ArrayList<Account>();
		for(boolean isValid = accountSet.next(); isValid; isValid = accountSet.next()) {
			Account acc = createAccountFromResultSet(accountSet);
			if(acc != null) result.add(acc);
		}
		return result;
	}
	
	private Account createAccountFromResultSet(ResultSet rs) throws SQLException {
		Account.Status approved = Account.Status.valueOf(rs.getString("APPROVED"));
		if(rs.getString("ACCOUNT_TYPE").equals("SAVINGS")) {
			
			return new SavingsAccount(rs.getLong("ACCOUNT_ID"),
										  rs.getDouble("BALANCE"), 
										  rs.getDouble("INTEREST_RATE"),
										  approved
										  );
		}
		else if(rs.getString("ACCOUNT_TYPE").equals("CHECKING")) {
			return new CheckingAccount(rs.getLong("ACCOUNT_ID"),
					                       rs.getDouble("BALANCE"),
					                       approved);
		}
		//Currently this should be impossible as there are two types of accounts
		return null;
	}








	


	

}
