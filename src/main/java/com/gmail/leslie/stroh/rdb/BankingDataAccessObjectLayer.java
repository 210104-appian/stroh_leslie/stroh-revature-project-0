package com.gmail.leslie.stroh.rdb;

import com.gmail.leslie.stroh.users.Customer;
import com.gmail.leslie.stroh.users.Employee;
import com.gmail.leslie.stroh.users.User;

import java.util.List;

import com.gmail.leslie.stroh.accounts.Account;

public interface BankingDataAccessObjectLayer {

	List<Customer> getCustomerWithPendingAccounts();
	List<Account> getAccountsFromUserId(Long id);
	User getUserFromLoginInfo(String role, String username, String password);
	Customer getCustomerFromUsername(String username);
	boolean checkUserNameAvailability(String username);
	boolean insertNewUser(String username, String password);
	boolean createNewAccount(long customerId, String accountType, Account newAccount);
	long getMaxUserId();
	long getMaxAccountId();
	boolean updateAccountValue(long accountID,double toValue);
	boolean updateAccountStatus(long accountID, Account.Status newStatus);
	
}
