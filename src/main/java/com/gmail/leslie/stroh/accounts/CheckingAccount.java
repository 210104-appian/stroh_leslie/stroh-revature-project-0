package com.gmail.leslie.stroh.accounts;

import java.util.Scanner;

public class CheckingAccount extends Account {
	
	public CheckingAccount() {
		super();
		//default
	}
	
	
	public CheckingAccount(long accountNumber,double balance, Status approved) {
		super(accountNumber,balance,approved);
	}

	@Override
	public String toString() {
		return "CheckingAccount [balance=" + balance + ", accountNumber=" + accountNumber + "]";
	}

	@Override
	public String outputAccountDetails() {
		return "AccountNumber:" + accountNumber + 
			   ", Type: Checking" + 
			   ", Balance:" + balance + 
			   ", Status: " + approved.name();
	}

	@Override
	public boolean getAccountInformationFromUser(Scanner scanner) {
		Double balance = getBalanceFromUser(scanner);
		if(balance == null) return false;
		setBalance(balance);
		return true;
	}
	
	
	
	
	
}
