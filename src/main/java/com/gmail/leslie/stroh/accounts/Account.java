package com.gmail.leslie.stroh.accounts;

import java.util.Scanner;

import com.gmail.leslie.stroh.util.ScannerInputUtil;

public abstract class Account {

	protected long accountNumber;
	protected double balance;
	public enum Status{ACCEPTED,REJECTED,PENDING};
	Status approved;
	
	
	protected Account() {
		accountNumber = -1;
		balance = 0.00;
		approved = Status.PENDING;
	}
	
	protected Account(long accountNumber, double balance, Status approved) {
		this.accountNumber = accountNumber;
		this.balance = balance;
		this.approved = approved;
	}
	
	public long getAccountNumber() {
		return accountNumber;
	}
	
	public double getBalance() {
		return balance;
	}

	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	};
	
	public Status getStatus() {
		return approved;
	}

	public void setStatus(Status approved) {
		this.approved = approved;
	}
	
	public abstract String outputAccountDetails();
	
	/*False means that the transaction was canceled, true means all information was gathered*/
	public abstract boolean getAccountInformationFromUser(Scanner scanner);
	
	
	protected Double getBalanceFromUser(Scanner scanner) {
		Double balance = ScannerInputUtil.getAPositiveDoubleFromScanner("Please enter an Initial balance or C to cancel",scanner);
		if(balance == null) return null;
		return balance;
	}
	
	
	
	
	
	
}
