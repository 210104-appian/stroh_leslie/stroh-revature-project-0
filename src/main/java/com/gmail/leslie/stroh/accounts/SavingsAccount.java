package com.gmail.leslie.stroh.accounts;

import java.util.Scanner;

import com.gmail.leslie.stroh.util.ScannerInputUtil;

public class SavingsAccount extends Account implements AccruableInterest{
	
	double interestRate;
	
	public SavingsAccount() {
		super();
	}
	
	public SavingsAccount(long accountNumber, double balance, double interestRate, Status approved) {
		super(accountNumber,balance,approved);
		this.interestRate = interestRate;
	}
	

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public double getInterestRate() {
		return interestRate;
	}

	@Override
	public void accrueInterest() {
		balance += interestRate * balance;
		
	}
	
	@Override
	public String toString() {
		return "SavingsAccount [interestRate=" + interestRate + ", accountNumber=" + accountNumber + ", balance="
				+ balance + "]";
	}

	@Override
	public String outputAccountDetails() {
		return "AccountNumber:" + accountNumber + 
			   ", Type: Savings" + 
			   ", Balance:" + balance + 
			   ", Interest Rate: " + interestRate +
			   ", Status: " + approved.name();
	}

	@Override
	public boolean getAccountInformationFromUser(Scanner scanner) {
		Double balance = getBalanceFromUser(scanner);
		if(balance == null) return false;
		Double interestRate = null;
		do {
			interestRate = ScannerInputUtil.getAPositiveDoubleFromScanner("Please enter the interest rate for your account or C to cancel",scanner);
			if(interestRate == null) return false;
			if(interestRate > 1) System.out.println("Value must be between 0 and 1 inclusive");
		}while(interestRate != null && interestRate > 1);
		setBalance(balance);
		setInterestRate(interestRate);
		return true;
	}

	

}
