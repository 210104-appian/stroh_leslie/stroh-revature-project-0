package com.gmail.leslie.stroh.accounts;

public interface AccruableInterest {

		double getInterestRate();
		void accrueInterest();
		
}
