package com.gmail.leslie.stroh.users;

import java.util.HashSet;

public interface User {
	
	long  getID();
	void  setID(long id);
	String getUserName();
	String getCredentials();
	void changeUserName(String username);
	void changeCredentials(String password);
	String displayUser();
	
}
