package com.gmail.leslie.stroh.users;
import java.util.ArrayList;
import java.util.List;

import com.gmail.leslie.stroh.accounts.Account;

public class Customer implements User {
	
	private long customerId;


	private List<Account> bankAccounts;
	private String userName;
	private String password;
	
	public Customer() {
		bankAccounts = new ArrayList<Account>();
	}
	
	public Customer(long id, String userName,String password) {
		this();
		this.userName = userName;
		this.password = password;
		this.customerId = id;
	}
	
	public String getUserName() {
		return userName;
	}

	public String getCredentials() {
		return password;
	}
	
	@Override
	public void changeCredentials(String password) {
		this.password = password;
	}

	@Override
	public void changeUserName(String userName) {
		this.userName = userName;
	}

	public List<Account> getBankAccounts() {
		return bankAccounts;
	}

	public void setBankAccounts(List<Account> bankAccounts) {
		this.bankAccounts = bankAccounts;
	}
	

	@Override
	public String toString() {
		return "Customer [bankAccounts=" + bankAccounts + ", userName=" + userName + ", password=" + password + "]";
	}

	@Override
	public long getID() {
		return customerId;
	}

	@Override
	public void setID(long id) {
		this.customerId = id;
		
	}

	@Override
	public String displayUser() {
		return "ID:" + customerId + ", Username:" + userName; 
		
	}
	
	
	
	
	

}
