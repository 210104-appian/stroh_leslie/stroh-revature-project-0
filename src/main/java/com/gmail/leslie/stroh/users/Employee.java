package com.gmail.leslie.stroh.users;

import java.util.HashSet;
import java.util.List;

import com.gmail.leslie.stroh.accounts.Account;

public class Employee implements User{
	String userName;
	String password;
	long employeeId;
	
	public Employee() {
		//Default
	}
	
	
	public Employee(long id, String userName, String password) {
		this.userName = userName;
		this.password = password;
		this.employeeId = id;
	}
	
	public String getUserName() {
		return userName;
	}

	public String getCredentials() {
		return password;
	}

	@Override
	public void changeUserName(String userName) {
		this.userName = userName;	
	}

	@Override
	public void changeCredentials(String password) {
		this.password = password;
	}
	
	
	public boolean approveAccount() {
		return true;
	}
	


	@Override
	public String toString() {
		return "Employee [userName=" + userName + ", password=" + password + "]";
	}


	@Override
	public long getID() {
		return employeeId;
	}


	@Override
	public void setID(long id) {
		this.employeeId = id;
	}


	@Override
	public String displayUser() {
		return "ID:" + employeeId + ", Username:" + userName; 
		
	}

	



}
