package com.gmail.leslie.stroh.frontend;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Stream;

import com.gmail.leslie.stroh.rdb.BankingDaoImpl;
import com.gmail.leslie.stroh.rdb.BankingDataAccessObjectLayer;
import com.gmail.leslie.stroh.users.Customer;
import com.gmail.leslie.stroh.users.Employee;
import com.gmail.leslie.stroh.users.User;

public class Session {
	private Scanner scanner;
	private BankingDataAccessObjectLayer database;
	private UserSession userSession;
	private String exitString;
	
	public Session() {
		scanner = new Scanner(System.in);
		scanner.useDelimiter("\n");
		database = new BankingDaoImpl();
		exitString = "Q";
	}
	
	public Session(String exitString, Scanner scanner){
		this();
		this.scanner = scanner;
		scanner.useDelimiter("\n");
		this.exitString = exitString;
	}
	
	
	/*
	 * Returns true if login was successful(will loop until successful) or false if exit string was entered;
	 */
	
	public boolean logIn() {
		System.out.println("Press 1 for Employee Login\nPress 2 for Customer Login\nPress 3 to Sign Up\nEnter " 
						    + exitString + " to quit the application");
		String roleInput = scanner.next().trim();
		
		if(roleInput.equals(exitString)) return false;
		
		if(roleInput.equals("3")) {
			signUpForAccount();
			//Once signed up go back to the login screen to login
			return logIn();
		}
		String role = "NONE";
		if(roleInput.equals("1"))
			role = "EMPLOYEE";
		else if(roleInput.equals("2"))
			role = "CUSTOMER";
			
		if(role.equals("NONE")) {
			System.out.println("Invalid input");
			return logIn();
		}
		
		System.out.print("Username: ");
		String username = scanner.next().trim();
		
		System.out.print("Password: ");
		String password = scanner.next().trim();
		
		//This method will return null if there do not exists credentials 
		//in the database with the correct type of role and login
		User user = database.getUserFromLoginInfo(role, username, password);
		if(user == null) {
			System.out.println("Username/password combination do not exist");
			return logIn();
		};
		setUserSession(role, user);
		return true;
	}


	//Returns false if the exit string was given, otherwise true
	public boolean runUserSession() {
		userSession.outputOptions();
		System.out.println("Enter "+ exitString + " to quit application");
		String input = scanner.next().trim();
		
		if(input.equals(exitString)) return false;
		
		userSession.runOption(input);
		return true;
	}
	
	
	private void signUpForAccount() {
		System.out.println("Press C to cancel transaction");
		System.out.print("Enter your desired Username: ");
		
		boolean hasSetValidUsername = false;
		boolean hasSetValidPassword = false;
		String curUsername = null;
		String curPassword = null;
		
		while(!hasSetValidUsername) {
			String userName = scanner.next().trim();
			if(userName.equals("C")) {
				System.out.println("Sign up aborted");
				return;
			}
			if(!checkPasswordAndUsernameValidity(userName)){
				System.out.println("The username must be between 1 and 40 characters, and contain only letters and digits");
				System.out.print("Enter your desired Username:");
			}
			else if(!database.checkUserNameAvailability(userName)) {
				System.out.println("The username you entered is unavailable");
				System.out.print("Enter your desired Username: ");
			}
			else {
				curUsername = userName;
				hasSetValidUsername = true;
			}
		}
		
		System.out.print("Enter your password:");
		while(!hasSetValidPassword) {
			String password = scanner.next().trim();
			if(password.equals("C")) {
				System.out.println("Sign up aborted");
				return;
			}
			if(!checkPasswordAndUsernameValidity(password)) {
				System.out.println("The password must be between 1 and 40 characters, and contain only letters and digits");
				System.out.print("Enter your desired password: ");
			}
			else {
				curPassword = password;
				hasSetValidPassword = true;
			}
		}
		
		boolean successful = database.insertNewUser(curUsername, curPassword);
		if(!successful) System.err.println("The login failed");
		else System.out.println("Username and password successfully created");
		
	}
	
	
	//Valid passwords and usernames will contain only letters or digits,
	//This is obviously not a common or possibly even security conscious requirement,
	//but it is simple to implement and would be easy to change.
	public boolean checkPasswordAndUsernameValidity(String input) {
		if(input.length() == 0 || input.length() > 40) return false;
		return !input.chars().anyMatch(character -> !Character.isLetterOrDigit(character));
	}
	
	private void setUserSession(String role,User user) {
		switch(role) {
			case "EMPLOYEE":{
				userSession = new EmployeeSession(user,scanner);
				break;
			}
			case "CUSTOMER":{
				userSession = new CustomerSession(user,scanner);
				break;
			}
			
		}
	}
	
	
	
	
	
	
	
	
}
