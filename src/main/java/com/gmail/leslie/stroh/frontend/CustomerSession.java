package com.gmail.leslie.stroh.frontend;

import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;
import com.gmail.leslie.stroh.accounts.Account;
import com.gmail.leslie.stroh.accounts.Account.Status;
import com.gmail.leslie.stroh.accounts.AccountOptions;
import com.gmail.leslie.stroh.accounts.CheckingAccount;
import com.gmail.leslie.stroh.accounts.SavingsAccount;
import com.gmail.leslie.stroh.rdb.BankingDaoImpl;
import com.gmail.leslie.stroh.rdb.BankingDataAccessObjectLayer;
import com.gmail.leslie.stroh.users.Customer;
import com.gmail.leslie.stroh.users.User;
import com.gmail.leslie.stroh.util.ScannerInputUtil;

public class CustomerSession implements UserSession{
	private Customer user;
	private BankingDataAccessObjectLayer database = new BankingDaoImpl();
	private Scanner scanner;
	private static Logger log = Logger.getRootLogger();
	
	CustomerSession(User customer,Scanner scanner){
		this.scanner = scanner;
		user = (Customer)customer;
	}
	
	@Override
	public void outputOptions() {
		String commandOptions = 
				"Customer Options\n"+
				"1) Apply for new bank account\n"+
			    "2) View accounts\n" +
				"3) Make a withdrawl of account\n"+
			    "4) Make a deposit of account\n";
				
		System.out.println(commandOptions);
		
	}

	@Override
	public void runOption(String input) {
		if(input.equals("1")) applyForNewBankAccount();
		else if(input.equals("2")) viewAccounts();
		else if(input.equals("3")) makeAWithdrawal();
		else if(input.equals("4")) makeADeposit();
		else System.out.println("Invalid Option");
	}
	
	private void applyForNewBankAccount() {
		AccountOptions accountType = selectAccountType();
		if(accountType == null) return;
		
		Account newAccount = null;
		switch(accountType) {
		case CHECKING:
			newAccount = new CheckingAccount();
			break;
		case SAVINGS:
			newAccount = new SavingsAccount();
			break;	
		}
		
		if(!newAccount.getAccountInformationFromUser(scanner)) return;
		
		boolean successful = database.createNewAccount(user.getID(), accountType.name(),newAccount);
		if(!successful) {
			System.out.println("Failed to create the account");
			log.error("Failed to create account: " + newAccount.outputAccountDetails());
		}
		else {
			user.setBankAccounts(database.getAccountsFromUserId(user.getID()));
			System.out.println("Successfully created account pending approval");
			log.info("Created new account for user: " + user.displayUser() + " Account: " + newAccount.outputAccountDetails());
		}
	}
	
	private void viewAccounts() {
		List<Account> accounts = user.getBankAccounts();
		showAccounts(accounts,"Account Information: press Enter to exit");
		scanner.next();
	}
	
	private void makeAWithdrawal() {
		List<Account> accounts = user.getBankAccounts()
				                     .stream()
                					 .filter((account) -> account.getStatus() == Status.ACCEPTED)
                					 .collect(Collectors.toList());
		
		showAccounts(accounts, "Select an account that you wish to withdrawal money from or enter C to cancel");
		Integer index = ScannerInputUtil.selectIndexFromListOneIndexed(accounts.size(),scanner);
		if(index == null) return;
		Account accountToWithdrawal = accounts.get(index - 1);
		Double amt;
		double accountToWithdrawalBalance = accountToWithdrawal.getBalance();
		do {
			 amt = ScannerInputUtil.getAPositiveDoubleFromScanner("Enter amount of Withdrawal or C to cancel",scanner);
			 if(amt == null) return;
			 if(amt > accountToWithdrawalBalance) System.out.println("Cannot withdrawal more than the balance of the account");
			 
		}while(amt > accountToWithdrawalBalance);
		
		boolean successful = database.updateAccountValue(accountToWithdrawal.getAccountNumber(),accountToWithdrawalBalance - amt);
		if(!successful) {
			System.out.println("Failed to update account");
			log.error("User: " + user.displayUser() + " failed to withraw " + amt + " from account: " + accountToWithdrawal.outputAccountDetails());
		}
		else {
			user.setBankAccounts(database.getAccountsFromUserId(user.getID()));
			System.out.println("Successfully updated account");
			log.info("User: " + user.displayUser() + " withdrew " + amt + " from account: " + accountToWithdrawal.outputAccountDetails());
		}
	}
	
	private void makeADeposit() {
		List<Account> accounts = user.getBankAccounts()
                			         .stream()
				                     .filter((account) -> account.getStatus() == Status.ACCEPTED)
				                     .collect(Collectors.toList());
		
		showAccounts(accounts, "Select an account that you wish to deposit money into or enter C to cancel");
		Integer index = ScannerInputUtil.selectIndexFromListOneIndexed(accounts.size(),scanner);
		if(index == null) return;
		Account accountToDeposit = accounts.get(index-1);
		
		double accountToDepositBalance = accountToDeposit.getBalance();
		Double amt = ScannerInputUtil.getAPositiveDoubleFromScanner("Enter amount of deposit or C to cancel",scanner);
		if(amt == null) return;
		
		boolean successful = database.updateAccountValue(accountToDeposit.getAccountNumber(),accountToDepositBalance + amt);
		if(!successful) {
			System.out.println("Failed to update account");
			log.error("User: " + user.displayUser() + " failed to deposit " + amt + " to account: " + accountToDeposit.outputAccountDetails());
		}
		else {
			user.setBankAccounts(database.getAccountsFromUserId(user.getID()));
			System.out.println("Successfully updated account");
			log.info("User: " + user.displayUser() + " deposited " + amt + " to account: " + accountToDeposit.outputAccountDetails());
		}
	}
	
	private void showAccounts(List<Account> accounts, String header) {
		System.out.println(header);
		if(accounts.size() == 0) System.out.println("No accounts to be shown");
		else {
			for(int i = 0; i < accounts.size(); i++) {
				System.out.println((i+1) + ")" + accounts.get(i).outputAccountDetails());
			}
		}
	}
	
	private AccountOptions selectAccountType() {
		AccountOptions[] values = AccountOptions.values();
		System.out.println("Select the type of account you would like to open or press C to cancel");
		for(int i = 0;i < values.length; i++) {
			System.out.println((i+1) + ") " + values[i]);
		}
		Integer index = ScannerInputUtil.selectIndexFromListOneIndexed(values.length,scanner);
		if(index == null) return null;
		return values[index - 1];
		
	}
	
	
	
	
	
	

	
	

	
}
	

