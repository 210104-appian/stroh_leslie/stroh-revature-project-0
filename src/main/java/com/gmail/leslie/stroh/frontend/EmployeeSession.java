package com.gmail.leslie.stroh.frontend;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import org.apache.log4j.Logger;

import com.gmail.leslie.stroh.accounts.Account;
import com.gmail.leslie.stroh.rdb.BankingDaoImpl;
import com.gmail.leslie.stroh.rdb.BankingDataAccessObjectLayer;
import com.gmail.leslie.stroh.users.Customer;
import com.gmail.leslie.stroh.users.Employee;
import com.gmail.leslie.stroh.users.User;
import com.gmail.leslie.stroh.util.ScannerInputUtil;

public class EmployeeSession implements UserSession {
	
	private Employee currentUser;
	private BankingDataAccessObjectLayer database = new BankingDaoImpl();
	private Scanner scanner;
	private static Logger log = Logger.getRootLogger();

	public EmployeeSession(User currentUser,Scanner scanner) {
		this.currentUser = (Employee)currentUser;
		this.scanner = scanner;
	}
	
	
	@Override
	public void outputOptions() {
		String commandOptions = 
			"1) Approve/Reject account\n"+
			"2) View customer's bank accounts\n" +
			"3) View log of transactions\n";
		System.out.println(commandOptions);
		
	}

	@Override
	public void runOption(String input) {
		if(input.equals("1")) approveRejectAccount();
		else if(input.equals("2")) viewCustomersBankAccounts();
		else if(input.equals("3")) viewLogOfTransactions();
		else System.out.println("Invalid Option");
	}


	private void approveRejectAccount() {
		List<Customer> customersWithPending = database.getCustomerWithPendingAccounts();
		showCustomers(customersWithPending,"Select which customer with pending accounts to be reviewed or enter C to cancel");
		Integer index = ScannerInputUtil.selectIndexFromListOneIndexed(customersWithPending.size(), scanner);
		if(index == null) return;
		
		List<Account> selectedCustomerAccounts = customersWithPending.get(index-1).getBankAccounts();
		showAccounts(selectedCustomerAccounts,"Select the account to be reviewed");
		Integer accountIndex = ScannerInputUtil.selectIndexFromListOneIndexed(selectedCustomerAccounts.size(), scanner);
		if(accountIndex == null) return;
		
		Account selectedAccount = selectedCustomerAccounts.get(accountIndex - 1);
		Account.Status reviewResult = getApprovalInput();
		if(reviewResult == null) return;
		
		boolean successful = database.updateAccountStatus(selectedAccount.getAccountNumber(), reviewResult);
		if(!successful) {
			System.err.println("Failed to update the status of the account");
			log.error("Failed to update the status of the account " + selectedAccount.outputAccountDetails());
		}
		else{
			System.out.println("Successfully updated status of account");
			log.info("Employee: " + currentUser.displayUser() + " has " + reviewResult.name() + " account " + selectedAccount.outputAccountDetails());
		}
		
	}


	private void viewCustomersBankAccounts() {
		
		Customer c = null;
		do {
			System.out.println("Please enter username of customer to view their accounts or Press C to cancel");
			String username = scanner.next().trim();
			if(username.equals("C"))return;
			c = database.getCustomerFromUsername(username);
			if(c == null) System.out.println("Failed to retrieve customer from username:" + username);
		}while(c == null);
		
		showAccounts(c.getBankAccounts(),"Account Information: press Enter to exit");
		scanner.next();
	}


	private void viewLogOfTransactions() {
		System.out.println("List of all logged transactions, Press Enter to exit");
		try(BufferedReader br = new BufferedReader(new FileReader("./src/main/resources/transaction_log.txt"));) {
			
			String line;
			while((line = br.readLine()) != null){
				System.out.println(line);
			}
			scanner.next();

		} catch (IOException e) {
			e.printStackTrace();
			log.error("Could not open up the transaction log file");
		}
		
	}
	
	private Account.Status getApprovalInput(){
		System.out.println("Enter 1 to ACCEPT, Enter 2 to REJECT or Enter C to cancel");
		String val = scanner.next().trim();
		if(val.equals("1")) return Account.Status.ACCEPTED;
		else if(val.equals("2")) return Account.Status.REJECTED;
		else if(val.equals("C")) return null;
		else{
			System.out.println("Invalid input");
			return getApprovalInput();
		}
	}
	
	private void showCustomers(List<Customer> customers, String header) {
		System.out.println(header);
		if(customers.size() == 0) System.out.println("No customers to be shown");
		else {
			for(int i = 0; i < customers.size(); i++) {
				System.out.println((i+1) + ")" + customers.get(i).displayUser());
			}
		}
	}
	
	private void showAccounts(List<Account> accounts, String header) {
		System.out.println(header);
		if(accounts.size() == 0) System.out.println("No accounts to be shown");
		else {
			for(int i = 0; i < accounts.size(); i++) {
				System.out.println((i+1) + ")" + accounts.get(i).outputAccountDetails());
			}
		}
	}
	

}
