package com.gmail.leslie.stroh;

import com.gmail.leslie.stroh.frontend.Session;
public class App{

	public static void main(String[] args) {
		Session s = new Session();
		boolean logInSuccessful = s.logIn();
		if(logInSuccessful) {
			boolean hasNotEnteredQuitPhrase = true;
			while(hasNotEnteredQuitPhrase) {
				hasNotEnteredQuitPhrase = s.runUserSession();
			}
		}
		
	}
}