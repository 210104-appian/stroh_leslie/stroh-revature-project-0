package com.gmail.leslie.stroh.util;

import java.util.Scanner;
import java.util.regex.Pattern;

public class ScannerInputUtil {
	
	
	public static Integer selectIndexFromListOneIndexed(int listLength, Scanner scanner) {
		String input = scanner.next().trim();
		if(input.equals("C")) return null;
		if(!isPositiveInteger(input)) {
			System.out.println("Must be a positive number between 1 and " + listLength);
			return selectIndexFromListOneIndexed(listLength,scanner);
		}
		int index = Integer.valueOf(input);
		if(index <= 0 || index > listLength) {
			System.out.println("Must be a positive number between 1 and " + listLength);
			return selectIndexFromListOneIndexed(listLength,scanner);
		}
		return index;
	}
	
	
	//Used wrapper class to return null if the cancel string is pressed
	public static Double getAPositiveDoubleFromScanner(String Header,Scanner scanner) {
		System.out.println(Header);
		double result = 0.00;
		boolean validResult = false;
		while(!validResult) {
			String balance = scanner.next().trim();
			if(balance.equals("C")) return null;
			if(stringIsDouble(balance)) {
				result = Double.valueOf(balance);
				if(result < 0) {
					System.out.println("Value must be greater than or equal to 0");
					System.out.print(Header);
				}
				else {
					validResult = true;
				}
			}
			else {
				System.out.println("Value must be a number >= 0");
				System.out.println(Header);
			}
		}
		return result;
	}
	
	
	//Contents of the method are copy and pasted from javadoc as a recommended way of making sure invalid argument is not
	//passed to Double.valueOf method.
	private static boolean stringIsDouble(String input) {
		final String Digits     = "(\\p{Digit}+)";
		  final String HexDigits  = "(\\p{XDigit}+)";
		  // an exponent is 'e' or 'E' followed by an optionally
		  // signed decimal integer.
		  final String Exp        = "[eE][+-]?"+Digits;
		  final String fpRegex    =
	      ("[\\x00-\\x20]*"+  // Optional leading "whitespace"
	       "[+-]?(" + // Optional sign character
	       "NaN|" +           // "NaN" string
	       "Infinity|" +      // "Infinity" string

	       // A decimal floating-point string representing a finite positive
	       // number without a leading sign has at most five basic pieces:
	       // Digits . Digits ExponentPart FloatTypeSuffix
	       //
	       // Since this method allows integer-only strings as input
	       // in addition to strings of floating-point literals, the
	       // two sub-patterns below are simplifications of the grammar
	       // productions from section 3.10.2 of
	       // The Java� Language Specification.

	       // Digits ._opt Digits_opt ExponentPart_opt FloatTypeSuffix_opt
	       "((("+Digits+"(\\.)?("+Digits+"?)("+Exp+")?)|"+

	       // . Digits ExponentPart_opt FloatTypeSuffix_opt
	       "(\\.("+Digits+")("+Exp+")?)|"+

	       // Hexadecimal strings
	       "((" +
	        // 0[xX] HexDigits ._opt BinaryExponent FloatTypeSuffix_opt
	        "(0[xX]" + HexDigits + "(\\.)?)|" +

	        // 0[xX] HexDigits_opt . HexDigits BinaryExponent FloatTypeSuffix_opt
	        "(0[xX]" + HexDigits + "?(\\.)" + HexDigits + ")" +

	        ")[pP][+-]?" + Digits + "))" +
	       "[fFdD]?))" +
	       "[\\x00-\\x20]*");// Optional trailing "whitespace"
		  return Pattern.matches(fpRegex, input);
	}
	
	private static boolean isPositiveInteger(String input) {
		return Pattern.matches("\\d+", input);
	}
	
}
