package com.gmail.leslie.stroh.frontend;


import org.junit.Assert;
import org.junit.Test;

public class ValidPasswordUsernameTest {
	
	@Test
	public void testEmpty() {
		Session s = new Session();
		Assert.assertFalse(s.checkPasswordAndUsernameValidity(""));
	}
	
	@Test
	public void testSingleCharacter(){
		Session s = new Session();
		Assert.assertTrue(s.checkPasswordAndUsernameValidity("c"));
	}
	
	@Test
	public void testSingleNumber(){
		Session s = new Session();
		Assert.assertTrue(s.checkPasswordAndUsernameValidity("0"));
	}
	
	@Test
	public void testInvalidCharacter() {
		Session s = new Session();
		Assert.assertFalse(s.checkPasswordAndUsernameValidity("@"));
	}
	
	@Test
	public void validString() {
		Session s = new Session();
		Assert.assertTrue(s.checkPasswordAndUsernameValidity("banana"));
	}
	
	@Test
	public void testInvalidString() {
		Session s = new Session();
		Assert.assertFalse(s.checkPasswordAndUsernameValidity("ban.ana"));
	}
	
	@Test 
	public void validStringSize40() {
		Session s = new Session();
		Assert.assertTrue(s.checkPasswordAndUsernameValidity("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));
	}
	
	@Test 
	public void validStringSize41() {
		Session s = new Session();
		Assert.assertFalse(s.checkPasswordAndUsernameValidity("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));
	}

}
