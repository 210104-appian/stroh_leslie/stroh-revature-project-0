package com.gmail.leslie.stroh.util;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import org.junit.Assert;
import org.junit.Test;


public class selectIndexFromListOneIndexedTest {

	@Test
	public void testZeroLengthList(){
		Scanner scanner = new Scanner("1\nC\n");
		List<Integer> lst = new ArrayList<Integer>();
		Integer a = ScannerInputUtil.selectIndexFromListOneIndexed(lst.size(), scanner);
		Assert.assertEquals(null, a);
	}
	
	@Test
	public void testOneLengthList(){
		Scanner scanner = new Scanner("1");
		Integer[] arr = {1};
		List<Integer> lst = Arrays.asList(arr);
		int a = ScannerInputUtil.selectIndexFromListOneIndexed(lst.size(), scanner);
		Assert.assertEquals(1, a);
	}
	
	@Test
	public void selectNegativeNumberFirstThenCancel(){
		Scanner scanner = new Scanner("-6\nC\n");
		Integer[] arr = {1,2,3,4,5,6,7,8};
		List<Integer> lst = Arrays.asList(arr);
		Integer a = ScannerInputUtil.selectIndexFromListOneIndexed(lst.size(), scanner);
		Assert.assertEquals(null, a);
	}
	
	@Test
	public void tooLargeThenValidIndex(){
		Scanner scanner = new Scanner("9\n1\n");
		Integer[] arr = {1,2,3,4,5,6,7,8};
		List<Integer> lst = Arrays.asList(arr);
		int a = ScannerInputUtil.selectIndexFromListOneIndexed(lst.size(), scanner);
		Assert.assertEquals(1, a);
	}
	
	@Test
	public void multipleIncorrectThenCorrect() {
		Scanner scanner = new Scanner("9\n-1\n10\n8\n");
		Integer[] arr = {1,2,3,4,5,6,7,8};
		List<Integer> lst = Arrays.asList(arr);
		int a = ScannerInputUtil.selectIndexFromListOneIndexed(lst.size(), scanner);
		Assert.assertEquals(8, a);
	}
}
