package com.gmail.leslie.stroh.util;
import java.util.Scanner;
import org.junit.Assert;
import org.junit.Test;

public class getAPositiveDoubleFromScannerTest {

	@Test
	public void testZero(){
		Scanner scanner = new Scanner("0\n");
		Double a = ScannerInputUtil.getAPositiveDoubleFromScanner("", scanner);
		Assert.assertEquals(0, a.doubleValue(),0);
	}
	
	@Test
	public void testDecimalValue(){
		Scanner scanner = new Scanner("1.932943218\n");
		Double a = ScannerInputUtil.getAPositiveDoubleFromScanner("", scanner);
		Assert.assertEquals(1.932, a.doubleValue(),0.001);
	}
	
	
	@Test
	public void testNegativeValueThenCancel(){
		Scanner scanner = new Scanner("-1\nC\n");
		Double a = ScannerInputUtil.getAPositiveDoubleFromScanner("", scanner);
		Assert.assertEquals(null, a);
	}
	
	
	@Test
	public void testNegativeDecimalThenValidDecimal(){
		Scanner scanner = new Scanner("-1.75\n1209.893\n");
		Double a = ScannerInputUtil.getAPositiveDoubleFromScanner("", scanner);
		Assert.assertEquals(1209.893, a.doubleValue(),0.001);
	}

}
