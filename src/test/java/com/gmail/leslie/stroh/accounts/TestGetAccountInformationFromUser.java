package com.gmail.leslie.stroh.accounts;
import java.util.Scanner;

import org.junit.Assert;
import org.junit.Test;

public class TestGetAccountInformationFromUser {

	@Test
	public void correctInputsSavingsAccount() {
		SavingsAccount account = new SavingsAccount();
		Scanner scanner = new Scanner("1000.00\n0.01\n");

		boolean filled = account.getAccountInformationFromUser(scanner);
		Assert.assertEquals(1000.00, account.getBalance(),0.001);
		Assert.assertEquals(0.01,account.getInterestRate(),0.001);
		Assert.assertTrue(filled);
	}
	
	@Test
	public void correctThenCancelInputSavingsAccount() {
		SavingsAccount account = new SavingsAccount();
		Scanner scanner = new Scanner("1000.00\nC\n");

		boolean filled = account.getAccountInformationFromUser(scanner);
		Assert.assertFalse(filled);
	}
	
	@Test
	public void correctThenTooBigThenCorrectInputSavingsAccount() {
		SavingsAccount account = new SavingsAccount();
		Scanner scanner = new Scanner("1000.00\n1.1\n0.5\n");

		boolean filled = account.getAccountInformationFromUser(scanner);
		Assert.assertEquals(1000.00, account.getBalance(),0.001);
		Assert.assertEquals(0.5,account.getInterestRate(),0.001);
		Assert.assertTrue(filled);
	}
	
	@Test
	public void correctThenNegativeThenCorrectInputSavingsAccount() {
		SavingsAccount account = new SavingsAccount();
		Scanner scanner = new Scanner("1000.00\n-0.04\n0.5\n");

		boolean filled = account.getAccountInformationFromUser(scanner);
		Assert.assertEquals(1000.00, account.getBalance(),0.001);
		Assert.assertEquals(0.5,account.getInterestRate(),0.001);
		Assert.assertTrue(filled);
	}
	
	@Test
	public void testCheckingAccountCorrect() {
		CheckingAccount account = new CheckingAccount();
		Scanner scanner = new Scanner("1000.00\n-0.04\n0.5\n");
		boolean filled = account.getAccountInformationFromUser(scanner);
		Assert.assertEquals(1000.00, account.getBalance(),0.001);
		Assert.assertTrue(filled);
	}
	
	@Test
	public void testCheckingAccountTwoNegativeInput() {
		CheckingAccount account = new CheckingAccount();
		Scanner scanner = new Scanner("-1000.00\n-0.04\n0.5\n");
		boolean filled = account.getAccountInformationFromUser(scanner);
		Assert.assertEquals(0.5, account.getBalance(),0.001);
		Assert.assertTrue(filled);
	}
	
	@Test
	public void testCheckingAccountTwoNegativeThenCancelInput() {
		CheckingAccount account = new CheckingAccount();
		Scanner scanner = new Scanner("-1000.00\n-0.04\nC\n");
		boolean filled = account.getAccountInformationFromUser(scanner);
		Assert.assertFalse(filled);
	}
	
	
	
}
